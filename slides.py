from imports import *


class CoordScreen(Scene):
    def construct(self):
        screen_grid = ScreenGrid()
        dot = Dot([1, 1, 0])
        self.add(screen_grid)
        self.play(FadeIn(dot))
        self.wait()


class TreeTest(Scene):
    def construct(self):
        test_tree = {
            "root": [
                {
                    "obj": Text("Hola mundo"),
                    "coord": [0,0],
                    "methods": [
                        lambda m: m.to_edge(UP)
                    ]
                },
                {
                    "left":[
                        {
                            "obj": Text("LEFT"),
                            "coord": [-4,0],
                        },
                        {
                            "left_0": [
                                {
                                    "obj": Text("L0"),
                                    "coord": [-4.5,-1]
                                }
                            ],
                            "left_1": [
                                {
                                    "obj": Text("L1"),
                                    "coord": [-3.5,-1]
                                },
                                {
                                    "left_1_0": [
                                        {
                                            "obj": Text("L10"),
                                            "coord": [-3.7,-2]
                                        }
                                    ]
                                }
                            ],
                        }
                    ],
                    "center":[
                        {
                            "obj": Text("CENTER"),
                            "coord": [0,0],
                        }
                    ],
                    "right":[
                        {
                            "obj": Text("RIGHT"),
                            "coord": [4,0],
                        },
                        {
                            "right_1": [
                                {
                                    "obj": Text("R0"),
                                    "coord": [4-1,-1]
                                }
                            ],
                            "right_2": [
                                {
                                    "obj": Text("R1"),
                                    "coord": [4+1,-1]
                                }
                            ],
                        }
                    ],
                }
            ]
        }
        tree = DiagramaArbol(test_tree)

        self.add(tree)

        self.wait()